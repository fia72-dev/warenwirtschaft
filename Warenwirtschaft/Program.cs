﻿using System;
using System.Text;

namespace Warenwirtschaft
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            var geschaeftskunde = new Geschaeftskunde("Senkowski", "Kai", "Elsdorf", 0.03);
            geschaeftskunde.BerechnePreiseanfrage(299.0);

            var kunde = new Kunde("Senkowski", "Kai", "Elsdorf");
            kunde.BerechnePreiseanfrage(299.0);

            Console.ReadKey();
        }
    }
}