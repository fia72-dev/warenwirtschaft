﻿using System;

namespace Warenwirtschaft
{
    public class Geschaeftskunde : Kunde
    {
        public Geschaeftskunde(string name, string vorname, string adresse, double rabattsatz)
            : base(name, vorname, adresse)
        {
            Rabattsatz = rabattsatz;
        }

        public double Rabattsatz { get; private set; }

        public override void BerechnePreiseanfrage(double nettopreis)
        {
            Console.WriteLine($"Ihre Preisanfrage für Artikel XY ergibt: {nettopreis * (1 - Rabattsatz)}\x20AC (+{nettopreis * 0.19}\x20AC  MWST)");
        }
    }
}