﻿using System;

namespace Warenwirtschaft
{
    public class Kunde
    {
        public Kunde(string name, string vorname, string adresse)
        {
            Name = name;
            Vorname = vorname;
            Adresse = adresse;
        }

        public string Name { get; private set; }
        public string Vorname { get; private set; }
        public string Adresse { get; private set; }

        public virtual void BerechnePreiseanfrage(double nettopreis)
        {
            Console.WriteLine($"Ihre Preisanfrage für Artikel XY ergibt: {nettopreis * 1.19}\x20AC (inklusive MWST)");
        }
    }
}